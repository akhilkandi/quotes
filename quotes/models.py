from django.db import models
from datetime import datetime

# Create your models here.
from mongoengine import *
from quotes_project.settings import _MONGODB_NAME

#connect(_MONGODB_NAME)
class Quote(Document):
    quote = StringField(max_length=500)
    #pub_date = DateTimeField(default = datetime.now())
    no = IntField()
    author = StringField(max_length=200)
    category = StringField(max_length=200)
