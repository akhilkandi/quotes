from django.urls import path
from .views import QuoteTableView, QuoteDetailView, QuoteHomeView,QuotePostView
from .models import Quote

urlpatterns = [
 #path ( '' , QuoteTableView.as_view (model=Quote), name = '' ),
  path('quote', QuoteTableView, name = 'quo'),
  path('postquote', QuotePostView, name = 'post-quote'),
  path('quote/(?P<pk>\d+)/$', QuoteDetailView, name = 'quote'),
  path('', QuoteHomeView),
]
