from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView,DetailView
from .models import Quote
#sfrom .forms import Quoteform
from django.contrib.auth import authenticate
from django.shortcuts import redirect


import mongoengine

#user = authenticate(username="akhil", password="Football1")
#assert isinstance(user, mongoengine.django.auth.User)

def QuoteTableView(request):

    #model = Quote
    quote = Quote(quote = "This too shall pass")
    quote.no = 1
    quote.author = "akhil"
    quote.category = "zen"
    quote.save()
    quotes = Quote.objects
    context ={'quotes':quotes}
    #print(quotes)
    return render(request,'quotes.html', context)
    #template = "quotes.html"

def QuoteDetailView(request, pk):
    quote = Quote.objects(pk = pk)
    #print(quote.no)
    context = {'quotes': quote}
    return render(request, 'quote_detail.html', context)

def QuoteHomeView(request):
    return render(request, 'home.html')

def QuotePostView(request):
    if request.method == "POST":
        #form = Quoteform(request.POST)
        #if (form.is_valid()):
            #quote = form.save()
            #quote.save()
            print("hey")
            dict= request.POST
            print(dict)

            quote =Quote(dict.get('examplequote'))
            quote.no = dict.get('exampleno')
            print(quote.no)
            quote.author= dict.get('exampleauthor')
            quote.category= dict.get('examplecategory')
            quote.save()

            return redirect('quote',pk = quote.pk )


    else:
        #form = Quoteform()
        return render(request, 'quote_post.html')
